// deep merge: https://gomakethings.com/merging-objects-with-vanilla-javascript/

const isObject = o => o && typeof o === 'object' && !Array.isArray(o);

const merge = (target, source) => {
  let result = Object.assign({}, target);
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(key => {
      if (isObject(source[key])) {
        if (!(key in target)) {
          Object.assign(result, { [key]: source[key] });
        }
        else {
          result[key] = merge(target[key], source[key]);
        }
      }
      else {
        Object.assign(result, { [key]: source[key] });
      }
    });
  }
  return result
};

const DEFAULT_TIMEOUT = 1000;

var createCache = () => {
  const store = {};
  let timeout = DEFAULT_TIMEOUT;
  return {
    expiresIn: milliseconds => timeout = milliseconds ? milliseconds : DEFAULT_TIMEOUT,
    has: uri => Boolean(store[uri]) && Date.now() - store[uri].last < timeout,
    get: uri => store[uri] ? JSON.parse(store[uri].data) : null,
    set: (uri, data) => {
      store[uri] = {
        data: JSON.stringify(data),
        last: Date.now(),
      };
      return Promise.resolve(data)
    },
  }
};

class HTTPError extends Error {
  constructor(url, response) {
    super(response.statusText);
    this.name = 'HTTPError';
    this.requestURL = url;
    this.response = response;
  }
}

class FetchParams {
  constructor(input, init) {
    this.input = input;
    this.init = init;
  }
}

// using some ideas from ky (https://github.com/sindresorhus/ky)

const defaultOptions = {
  baseURL: '',
  headers: {},
  autoJson: true,
  cached: true,
  beforeRequest: [],
  afterResponse: [],
};

const createApiInstance = options => {
  const instanceOptions = merge(defaultOptions, options || {});

  // init cache
  let instanceCache = instanceOptions.cached ? createCache() : null;
  delete instanceOptions.cached;
  if (instanceCache && instanceOptions.cacheLife) {
    instanceCache.expiresIn(instanceOptions.cacheLife);
    delete instanceOptions.cacheLife;
  }

  const getData = async response => {
    const isJson = /json/.test(response.headers.get('Content-Type'));
    return isJson && instanceOptions.autoJson ? response.json() : response.body
  };

  const apiCall = async (method, resource, params = null, body = null, options = {}) => {
    let fetchOptions = merge(instanceOptions, options);
    fetchOptions.method = method;

    // build url
    params = params ? '?' + new URLSearchParams(params).toString() : '';
    let url = `${fetchOptions.baseURL}${resource}${params}`;

    // handle body for json
    if (body) {
      // if a specific Content-Type header is specified, do not override the body
      const contentType = fetchOptions.headers['Content-Type'];
      if (fetchOptions.autoJson && (contentType === 'application/json' || !contentType)) {
        fetchOptions.body = JSON.stringify(body);
        fetchOptions.headers['Content-Type'] = 'application/json';
      }
      else {
        fetchOptions.body = body;
      }
    }

    // cleanup Api specific options to keep only fetch ones
    delete fetchOptions.baseURL;
    delete fetchOptions.autoJson;
    delete fetchOptions.beforeRequest;
    delete fetchOptions.afterResponse;

    // request interceptors
    for (const interceptor in instanceOptions.beforeRequest) {
      const result = await instanceOptions.beforeRequest[interceptor](url, fetchOptions);
      if (result instanceof Response) {
        return getData(result)
      }
      else if (result instanceof FetchParams) {
        url = result.input;
        fetchOptions = result.init;
      }
    }

    // check cache
    if (method === 'GET' && instanceCache && instanceCache.has(url)) {
      return instanceCache.get(url)
    }

    // do the fetch!
    let response = await fetch(url, fetchOptions);

    // response interceptors
    for (const interceptor in instanceOptions.afterResponse) {
      response = await instanceOptions.afterResponse[interceptor](url, fetchOptions, response.clone());
    }

    // handle HTTP errors
    if (!response.ok) {
      throw new HTTPError(url, response)
    }

    // handle json responses
    const data = getData(response);

    // update instance cache
    if (method === 'GET' && instanceCache) {
      instanceCache.set(url, data);
    }

    return data
  };

  return Object.freeze({
    setBaseURL: baseURL => {
      instanceOptions.baseURL = baseURL;
    },
    setAutoJson: enabled => {
      instanceOptions.autoJson = enabled;
    },
    setCached: enabled => {
      instanceCache = enabled ? createCache() : null;
    },
    setCacheLife: milliseconds => {
      if (instanceCache) {
        instanceCache.expiresIn(milliseconds);
      }
    },
    updateAuthorization: (token, type) => {
      if (!type) {
        type = 'Bearer ';
      }
      delete instanceOptions.headers['Authorization'];
      if (token && token !== false) {
        instanceOptions.headers['Authorization'] = `${type} ${token}`;
      }
    },

    get: (resource, params, options) => apiCall('GET', resource, params, null, options),
    post: (resource, params, body, options) => apiCall('POST', resource, params, body, options),
    put: (resource, params, body, options) => apiCall('PUT', resource, params, body, options),
    patch: (resource, params, body, options) => apiCall('PATCH', resource, params, body, options),
    head: (resource, params, options) => apiCall('HEAD', resource, params, null, options),
    delete: (resource, params, options) => apiCall('DELETE', resource, params, null, options),
    request: (method, resource, params, body, options) => apiCall(method, resource, params, body, options),
  })
};

const defaultApi = createApiInstance({});

export default defaultApi;
export { FetchParams, HTTPError, createApiInstance };
