module.exports = {
  root: true,
  extends: [ 'eslint:recommended' ],
//  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  rules: {
    'linebreak-style': 'off', // don't matter line ending style
    'indent': ['error', 2], // indent with 2 spaces
    'quotes': ['error', 'single'], // force single quotes
    'semi': ['error', 'never'], // remove semicolons
    'eqeqeq': 'warn', // require === and !==
    'default-case': 'warn', // require default case in switch statements
    'no-implicit-coercion': 'warn', // disallows implicit type conversion methods
    'no-magic-numbers': ['warn', { // forbid magic numbers (force declaring const)
      'ignore': [ -1, 0, 1, 2 ]
    }],
    'yoda': 'warn', // requires 'yoda' condition statements
    'no-var': 'warn', // requires let or const, not var
    'no-debugger': 'warn'
  }
}