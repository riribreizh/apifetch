import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import pkg from './package.json'

export default [
  {
    input: pkg.source,
    output: {
      format: 'umd',
      name: 'apifetch',
      file: pkg.browser,
    },
    plugins: [
      resolve(),
      commonjs(),
    ],
  },
  {
    input: pkg.source,
    external: 'deepmerge',
    output: [
      { file: pkg.main, format: 'cjs' },
      { file: pkg.module, format: 'es' },
    ],
  }
]
