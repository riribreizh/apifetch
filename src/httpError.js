export class HTTPError extends Error {
  constructor(url, response) {
    super(response.statusText)
    this.name = 'HTTPError'
    this.requestURL = url
    this.response = response
  }
}
