// deep merge: https://gomakethings.com/merging-objects-with-vanilla-javascript/

const isObject = o => o && typeof o === 'object' && !Array.isArray(o)

const merge = (target, source) => {
  let result = Object.assign({}, target)
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(key => {
      if (isObject(source[key])) {
        if (!(key in target)) {
          Object.assign(result, { [key]: source[key] })
        }
        else {
          result[key] = merge(target[key], source[key])
        }
      }
      else {
        Object.assign(result, { [key]: source[key] })
      }
    })
  }
  return result
}
export default merge
