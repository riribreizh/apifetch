const DEFAULT_TIMEOUT = 1000

export default () => {
  const store = {}
  let timeout = DEFAULT_TIMEOUT
  return {
    expiresIn: milliseconds => timeout = milliseconds ? milliseconds : DEFAULT_TIMEOUT,
    has: uri => Boolean(store[uri]) && Date.now() - store[uri].last < timeout,
    get: uri => store[uri] ? JSON.parse(store[uri].data) : null,
    set: (uri, data) => {
      store[uri] = {
        data: JSON.stringify(data),
        last: Date.now(),
      }
      return Promise.resolve(data)
    },
  }
}
